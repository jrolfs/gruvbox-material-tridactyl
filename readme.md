<div align="center">
  <h1>gruvbox-material-tridactyl</h1>
  <p>A <strong><a href="https://tridactyl.xyz/">Tridactyl</a></strong> theme based on the wonderful <a href="https://github.com/sainnhe/gruvbox-material">Gruvbox Material</a> color palette.</p>

  <br />
  
</div>

<img src="https://user-images.githubusercontent.com/288160/164817704-e34faff9-4538-48e0-a59f-0200ed61a3c6.png" alt="Dark Soft Screenshot">

¹This is a work in progress — 🚧 — so far only the **Dark Soft** variant has been implemented.

²You can find the Firefox theme used in the screenshot [here](https://addons.mozilla.org/en-US/firefox/addon/gruvbox-material-dark-soft/).

#### Credits
This was based originally off of [base16-tridactyl](https://github.com/bezmi/base16-tridactyl), 🙏🏻 thanks **[@bezmi](https://github.com/bezmi)** 